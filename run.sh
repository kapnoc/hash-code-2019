#!/bin/bash
mkdir -p out/
for f in $(find tests -iname "*.in")
do
	time stack run $f > $f.out
done
