module Main where

import Lib
import System.Environment
import Debug.Trace

main :: IO ()
main = do
  args <- getArgs
  redirArgs args

printSlide (idx, pic) = do
  if length pic > 1 then putStr $ (show $ pic !! 0) ++ " " else return ()
  putStrLn $ show $ index $ last pic

printResults :: [[Picture]] -> IO ()
printResults slides = do
  putStrLn $ show $ length slides
  mapM_ (printSlide) $ zip [0..] slides

redirArgs [file] = do
  contents <- readFile file
  printResults $ algo (parseFile contents) []
redirArgs _ = putStrLn "Need 1 arg: filename"


algo :: [Picture] -> [[Picture]] -> [[Picture]]
algo [] res = res
--algo pictures@(x:xs) [] = let closest = findClosestPicture x xs in (deleteAt closest xs) $ x ++ (xs !! closest)
algo pictures@(x:xs) res = trace (show $ algo deleted toAdd) $ algo deleted toAdd
                            where
                                closest = findClosestPicture x xs
                                deleted
                                    | length closest == 2 = deleteAt (head closest) $ deleteAt (closest !! 1) xs
                                    | otherwise = deleteAt (head closest) xs
                                toAdd
                                    | length closest == 2 = res ++ [[x]] ++ [[xs !! (head closest)] ++ [xs !! (closest !! 1)]]
                                    | otherwise = res ++ [[x]] ++ [[xs !! (head closest)]]
