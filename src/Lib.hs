module Lib
    ( someFunc
    , parseFile
    , Picture (..)
    , Orientation (..)
    , findClosestPicture
    , deleteAt
    ) where

import Data.List (splitAt, maximumBy, elemIndex)
import Data.Function (on)

import Debug.Trace

data Orientation = Horiz | Verti deriving (Show, Eq)

instance Read Orientation where
  readsPrec _ ('H':cs) = [(Horiz, cs)]
  readsPrec _ ('V':cs) = [(Verti, cs)]
  readsPrec _ _ = []

data Picture = Picture { index :: Int
                       , orientation :: Orientation
                       , tags :: [String]
                       } deriving (Show, Eq)

instance Read Picture where
  readsPrec _ (c:cs) = [((Picture 0 ori tags), [])]
    where
      ori = read [c]
      tags = tail $ words cs

someFunc :: IO ()
someFunc = putStrLn "someFunc"

parseFile :: String -> [Picture]
parseFile contents = zipWith (\i (Picture _ o t) -> Picture i o t ) [0..] $ map read $ tail $ lines contents


slice from to xs = take (to - from + 1) (drop from xs)

calcInterest x y = getComTag (tags x) (tags y)

getComTag :: [String] -> [String] -> Int
getComTag [] _ = 0
getComTag _ [] = 0
getComTag (x:xs) b = getComTag xs b + (if x `elem` b then 1 else 0)

nbMatching :: [String] -> [String] -> Int
nbMatching a b = foldl (\acc x -> if x `elem` b then succ acc else acc) 0 a

findClosestPicture :: Picture -> [Picture] -> [Int]
findClosestPicture e list = let closest e list = maximumBy (compare `on` (\x -> nbMatching (tags e) (tags x))) list
  in case elemIndex (closest e list) list of
    Just i -> case closest e list of
      Picture _ Horiz _ -> [i]
      Picture idx Verti _ -> let filtered = filter (\x -> orientation x == Verti && index x /= idx) in
        if length (filtered list) > 0
            then [i, (case elemIndex (closest e $ filtered list) list of Just i -> i)]
            else [-1]
    _ -> [-1]

deleteAt idx xs = lft ++ rgt
  where (lft, (_:rgt)) = splitAt idx xs
